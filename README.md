E-SCARA
=============
## 1. 하드웨어
-------------
하드웨어는 [DrawBot](https://www.thingiverse.com/thing:3096135, "drawbot link")를 참조하여 만들었다.   
* 사용 부품   
>    Ardoino Mega 2560   
>    Ramps 1.4   
>    Raspberry Pi 3B+   
>    Step Motor x2   
>    Servo Motor   

국내에서 구할 수 있는 타이밍벨트 길이에 맞게 stl파일들을 수정하였다.
또한 바닥에 단단히 고정할 수 있게 받침대에 나사구멍을 만들었다.
<img src="https://gitlab.com/skku-sior/2019/e-scara/images/ESCARA.jpg" width="450px" height="300px" title="ESCARA" alt="ESCARA"></img><br/>

## 2. 오픈소스 소프트웨어   
-------------
### 2-1. 수정 GRBL 펌웨어   
    G-code를 아두이노에 보냈을 때 스텝모터가 해당 코드에 대응하는 동작을 하게끔 함.   
    [DrawBot](https://www.thingiverse.com/thing:3096135/)에서 수정해놓은 GRBL 펌웨어를 Xloader을 이용하여 Ardoino Mega 2560에 업로드했다.   
### 2-2. Pronterface   
    간단하게 G-code 전송을 통한 로봇 동작 확인을 할 때 사용하였다.   
    링크 : [Pronterface](https://www.pronterface.com/)   
### 2-3. LaserGRBL   
    이미지 파일을 간단하게 G-code로 변환할 때 사용하였다.   
    링크 : [LaserGRBL](http://lasergrbl.com/)   
### 2-4. Inkscape   
    마찬가지로 이미지 파일을 G-code로 변환할 때 사용하였다.   
    링크 : [Inkscape](https://inkscape.org/ko/)  
### 2-5. Gcode-CLI
    CLI에서 G-code를 간편하게 보낼 수 있게 도와준다.
    링크 : [Gcode-CLI](https://github.com/hzeller/gcode-cli/)

## 3. 자작 소프트웨어   
-------------
### 3-1. Web Application Controller   
    node.js Express 기반 Web Application Controller   
    조이스틱 버튼을 누르면 상하좌우로 로봇팔이 움직이고   
    특수기능 버튼을 누르면 그에 해당하는 동작을 한다.   
    아래에 있는 그림을 누르면 로봇이 그 그림을 그려준다.   
<img src="https://gitlab.com/skku-sior/2019/e-scara/images/ESCARA_APP.jpg" width="450px" height="300px" title="ESCARA_APP" alt="ESCARA_APP"></img><br/>